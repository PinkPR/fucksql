CREATE OR REPLACE VIEW
V_AVATAR
  AS
    SELECT  account.acc_login,
            account.acc_name,
            account.acc_surname,
            avatar.av_name,
            avatar.av_exp,
            avatar.av_level
      FROM account, avatar
    WHERE account.acc_login = avatar.acc_login
      AND avatar.av_actif;

CREATE OR REPLACE VIEW
V_INGAME
  AS
    SELECT  avatar.acc_login  AS acc_login,
            avatar.av_name    AS av_name,
            mob.mob_pos_x     AS av_pos_x,
            mob.mob_pos_y     AS av_pos_y
      FROM avatar, playingavatar, mob
    WHERE avatar.av_id = playingavatar.av_id
      AND mob.mob_id = playingavatar.mob_id;

CREATE OR REPLACE VIEW
V_RACE_CLASS
  AS
    SELECT  foo.race_key_name,
            foo.cls_key_name,
            foo.required_xp_by_level,
            foo.bonus_hp_by_level,
            foo.bonus_cp_by_level
      FROM (SELECT  race.race_key_name AS race_key_name,
            class.cls_key_name AS cls_key_name,
            race.race_exp_by_level + class.cls_exp_by_level
              AS required_xp_by_level,
            race.race_bonus_hp_by_level + class.cls_bonus_hp_by_level
              AS bonus_hp_by_level,
            race.race_bonus_cp_by_level + class.cls_bonus_cp_by_level
              AS bonus_cp_by_level
      FROM race, class) AS foo
      LEFT JOIN restriction_class_race
        ON foo.race_key_name = restriction_class_race.race_key_name
          AND foo.cls_key_name = restriction_class_race.cls_key_name
      WHERE restriction_class_race.cls_key_name IS NULL
        OR restriction_class_race.race_key_name IS NULL
      ORDER BY  required_xp_by_level DESC,
                bonus_hp_by_level DESC,
                bonus_cp_by_level DESC;
