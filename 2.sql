CREATE OR REPLACE FUNCTION
F_ACCOUNT_ACTIF(P_ACC_LOGIN   char(15))
  RETURNS BOOLEAN AS
$$
  BEGIN
    IF (SELECT COUNT(av_name)
          FROM AVATAR
          WHERE ACC_LOGIN = P_ACC_LOGIN AND AV_ACTIF) = 1 THEN
      RETURN TRUE;
    END IF;
    RETURN FALSE;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION
F_PROGRESSION(P_RACE  char(5),
              P_CLASS char(5),
              P_LMIN  INTEGER,
              P_LMAX  INTEGER)
  -- RETURNS TABLE(INTEGER, INTEGER, INTEGER, INTEGER) AS
  /*RETURNS TABLE(a INTEGER,
                b INTEGER,
                c INTEGER,
                d INTEGER) AS*/
  RETURNS TABLE(a INTEGER,
                b INTEGER,
                c INTEGER,
                d INTEGER) AS
$$
  BEGIN
    FOR i in P_LMIN .. P_LMAX LOOP
      RETURN QUERY
        SELECT i,
               i * required_xp_by_level,
               race_hp_init_value + cls_hp_init_value + ((i - 1) * bonus_hp_by_level)::INTEGER,
               race_cp_init_value + cls_cp_init_value + ((i - 1) * bonus_cp_by_level)::INTEGER
          FROM V_RACE_CLASS, RACE, CLASS
          WHERE V_RACE_CLASS.race_key_name = P_RACE
            AND V_RACE_CLASS.cls_key_name = P_CLASS
            AND RACE.race_key_name = P_RACE
            AND CLASS.cls_key_name = P_CLASS;
    END LOOP;
  END;
$$ LANGUAGE plpgsql;
