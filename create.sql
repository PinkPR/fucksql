\echo CHARGE SCHEMA
-- table account pour enregistrer les joueurs
create table ACCOUNT (
        ACC_LOGIN           char(15) NOT NULL,
        ACC_NAME            char(32) NOT NULL,
        ACC_SURNAME         varchar(25) NOT NULL,
        ACC_PWD             varchar(25) NOT NULL,
        ACC_MAIL            text NOT NULL,
        CONSTRAINT          "An account have a primary key" primary key(ACC_LOGIN)
);

-- un avatar est d'une certaine race
--      * un nom long
--      * l'expérience (EXP) nécessaire pour prendre un niveau
--      * valeur initiale de point de vie (HP .. Hit Point)
--      * valeur initiale de point de compétence (CP)
--      * valeur a multiplier par le niveau réel est à ajouté aux HP initiaux
--      * valeur a multiplier par le niveau réel est à ajouté aux CP initiaux
create table RACE (
        RACE_KEY_NAME           char(5) NOT NULL,
        RACE_LONG_NAME          char(32) NOT NULL,
        RACE_EXP_BY_LEVEL       integer NOT NULL,
        RACE_HP_INIT_VALUE      integer NOT NULL,
        RACE_CP_INIT_VALUE      integer NOT NULL,
        RACE_BONUS_HP_BY_LEVEL  float NOT NULL,
        RACE_BONUS_CP_BY_LEVEL  float NOT NULL,
        CONSTRAINT              "A race have a primary key" primary key(RACE_KEY_NAME)
);

-- un avatar est d'une certaine classe
--      * un nom long
--      * l'expérience (EXP) nécessaire pour prendre un niveau
--      * valeur initiale de point de vie (HP .. Hit Point)
--      * valeur initiale de point de compétence (CP)
--      * valeur a multiplier par le niveau réel est à ajouté aux HP initiaux
--      * valeur a multiplier par le niveau réel est à ajouté aux CP initiaux
create table CLASS (
        CLS_KEY_NAME            char(5) NOT NULL,
        CLS_LONG_NAME           char(32) NOT NULL,
        CLS_EXP_BY_LEVEL        integer NOT NULL,
        CLS_HP_INIT_VALUE       integer NOT NULL,
        CLS_CP_INIT_VALUE       integer NOT NULL,
        CLS_BONUS_HP_BY_LEVEL   float NOT NULL,
        CLS_BONUS_CP_BY_LEVEL   float NOT NULL,
        CONSTRAINT              "A class have a primary key" primary key(CLS_KEY_NAME)
);

-- il existe des restrictions classe/race
-- on ne peut pas créer d'avatar ayant une CLASSE/RACE presentent dans cette table
create table RESTRICTION_CLASS_RACE (
        RCR_DESC                text NOT NULL,
        CLS_KEY_NAME            char(5) NOT NULL,
        CONSTRAINT              "A restriction belongs to a class" foreign key(CLS_KEY_NAME)
                                REFERENCES CLASS(CLS_KEY_NAME) DEFERRABLE INITIALLY DEFERRED,
        RACE_KEY_NAME           char(5) NOT NULL,
        CONSTRAINT              "A restriction belongs to a race" foreign key(RACE_KEY_NAME)
                                REFERENCES RACE(RACE_KEY_NAME) DEFERRABLE INITIALLY DEFERRED
);

-- un user incarne plusieur avatar avec :
--      * nombre de piece d'or PO de l'avatar
--      * experience de l'avatar
--      * level de l'avatar
--      * HP à la dernière connection de l'avatar
--      * Competence Point à la dernière connection
create table AVATAR (
        AV_ID               serial NOT NULL,
        AV_NAME             char(32) NOT NULL,
        AV_ACTIF            boolean DEFAULT FALSE,
        AV_CURRENT_PO       integer,
        AV_EXP              integer NOT NULL,
        AV_LEVEL            integer NOT NULL,
        AV_LAST_HP          integer NOT NULL,
        AV_LAST_CP          integer NOT NULL,
        CONSTRAINT          "An avatar have a primary key" primary key(AV_ID),
        ACC_LOGIN           char(15) NOT NULL,
        CONSTRAINT          "An avatar belongs to an account" foreign key(ACC_LOGIN)
                            REFERENCES ACCOUNT(ACC_LOGIN) DEFERRABLE INITIALLY DEFERRED,
        RACE_KEY_NAME       char(5) NOT NULL,
        CONSTRAINT          "An avatar belongs to a race" foreign key(RACE_KEY_NAME)
                            REFERENCES RACE(RACE_KEY_NAME) DEFERRABLE INITIALLY DEFERRED,
        CLS_KEY_NAME        char(5) NOT NULL,
        CONSTRAINT          "An avatar belongs to a class" foreign key(CLS_KEY_NAME)
                            REFERENCES CLASS(CLS_KEY_NAME) DEFERRABLE INITIALLY DEFERRED
);

-- competence generique
--      * nom long
--      * description de la competence
--      * information pour le Game Master concernant les régles associés à cette competence
create table COMPETENCE (
        COMP_KEY_NAME           char(5) NOT NULL,
        COMP_LONG_NAME          char(32) NOT NULL,
        COMP_DESC               text,
        COMP_INFO_GM            text,
        CONSTRAINT              "A competence have a primary key" primary key(COMP_KEY_NAME)
);

-- effet des competences
--      * niveau minimum requis pour utiliser la compétence
--      * valeur minimum de la compétence
--      * valeur a multiplier par le niveau réel est à ajouté à la valeur initial de la compétence
--      * point de compétence utilisé à chaque utilisation
create table COMPETENCE_FX (
        CFX_ID                  serial NOT NULL,
        CFX_MIN_LEVEL_REQ       integer NOT NULL,
        CFX_INIT_VALUE          integer NOT NULL,
        CFX_BONUS_BY_LEVEL      float NOT NULL,
        CFX_CP_BY_USE           integer NOT NULL,
        CONSTRAINT              "A competence_fx have a primary key" primary key(CFX_ID),
        COMP_KEY_NAME           char(5) NOT NULL,
        CONSTRAINT              "A competence_fx belongs to a competence" foreign key(COMP_KEY_NAME)
                                REFERENCES COMPETENCE(COMP_KEY_NAME) DEFERRABLE INITIALLY DEFERRED
);

-- il y a des competences de race
create table COMPETENCE_RACE (
        CFX_ID                  integer NOT NULL,
        CONSTRAINT              "A competence of race belongs to a competence" foreign key(CFX_ID)
                                REFERENCES COMPETENCE_FX(CFX_ID) DEFERRABLE INITIALLY DEFERRED,
        RACE_KEY_NAME           char(5) NOT NULL,
        CONSTRAINT              "A competence of race belongs to a race" foreign key(RACE_KEY_NAME)
                                REFERENCES RACE(RACE_KEY_NAME) DEFERRABLE INITIALLY DEFERRED,
        CONSTRAINT              "A competence of race have a primary key" primary key(CFX_ID, RACE_KEY_NAME)
);
create view FX_RACE as select * from COMPETENCE_FX natural inner join COMPETENCE_RACE;
create function FX_RACE_ADD(
                    IN MIN_LEVEL_REQ integer,
                    IN INIT_VALUE integer,
                    IN BONUS_BY_LEVEL float,
                    IN CP_BY_USE integer,
                    IN COMP_KEY_NAME char(5),
                    IN RACE_KEY_NAME char(5)
                ) RETURNS void AS $$
DECLARE
    ret RECORD;
    cfx_lastid integer;
BEGIN
    insert into COMPETENCE_FX (CFX_MIN_LEVEL_REQ, CFX_INIT_VALUE, CFX_BONUS_BY_LEVEL, CFX_CP_BY_USE, COMP_KEY_NAME) values
            (MIN_LEVEL_REQ, INIT_VALUE, BONUS_BY_LEVEL, CP_BY_USE, COMP_KEY_NAME) returning CFX_ID into cfx_lastid;
    insert into COMPETENCE_RACE values (cfx_lastid, RACE_KEY_NAME);
END;
$$ LANGUAGE plpgsql;
create rule FX_RACE_INSERT as on INSERT to FX_RACE
    do INSTEAD
    (
        select count(FX_RACE_ADD(NEW.CFX_MIN_LEVEL_REQ, NEW.CFX_INIT_VALUE, NEW.CFX_BONUS_BY_LEVEL, NEW.CFX_CP_BY_USE, NEW.COMP_KEY_NAME, NEW.RACE_KEY_NAME));
    );

-- il y a des competences de classe
create table COMPETENCE_CLS (
        CFX_ID                  integer NOT NULL,
        CONSTRAINT              "A competence of class belongs to a competence" foreign key(CFX_ID)
                                REFERENCES COMPETENCE_FX(CFX_ID) DEFERRABLE INITIALLY DEFERRED,
        CLS_KEY_NAME            char(5) NOT NULL,
        CONSTRAINT              "A competence of class belongs to a class" foreign key(CLS_KEY_NAME)
                                REFERENCES CLASS(CLS_KEY_NAME) DEFERRABLE INITIALLY DEFERRED,
        CONSTRAINT              "A competence of class have a primary key" primary key(CFX_ID, CLS_KEY_NAME)
);
create view FX_CLS as select * from COMPETENCE_FX natural inner join COMPETENCE_CLS;
create function FX_CLS_ADD(
                    IN MIN_LEVEL_REQ integer,
                    IN INIT_VALUE integer,
                    IN BONUS_BY_LEVEL float,
                    IN CP_BY_USE integer,
                    IN COMP_KEY_NAME char(5),
                    IN CLS_KEY_NAME char(5)
                ) RETURNS void AS $$
DECLARE
    ret RECORD;
    cfx_lastid integer;
BEGIN
    insert into COMPETENCE_FX (CFX_MIN_LEVEL_REQ, CFX_INIT_VALUE, CFX_BONUS_BY_LEVEL, CFX_CP_BY_USE, COMP_KEY_NAME) values
            (MIN_LEVEL_REQ, INIT_VALUE, BONUS_BY_LEVEL, CP_BY_USE, COMP_KEY_NAME) returning CFX_ID into cfx_lastid;
    insert into COMPETENCE_CLS values (cfx_lastid, CLS_KEY_NAME);
END;
$$ LANGUAGE plpgsql;
create rule FX_CLS_INSERT as on INSERT to FX_CLS
    do INSTEAD
    (
        select count(FX_CLS_ADD(NEW.CFX_MIN_LEVEL_REQ, NEW.CFX_INIT_VALUE, NEW.CFX_BONUS_BY_LEVEL, NEW.CFX_CP_BY_USE, NEW.COMP_KEY_NAME, NEW.CLS_KEY_NAME));
    );

-- terrain de jeu, le comment on passe d'une carte a une autre est hors propos (portes, teleporteur etc...)
create table MAP (
    MAP_ID                      serial NOT NULL,
    MAP_WIDTH                   integer NOT NULL,
    MAP_HEIGHT                  integer NOT NULL,
    MAP_DESC                    text NOT NULL,
    CONSTRAINT                  "A Map have a primary key" primary key(MAP_ID)
);

-- Les catégories de monstre
create table MONSTER (
    MON_KEY_NAME                char(10) NOT NULL,
    MON_EXP_DEAD                integer,
    MON_HP_MIN                  integer,
    MON_HP_MAX                  integer,
    MON_CP_MIN                  integer,
    MON_CP_MAX                  integer,
    CONSTRAINT                  "A Monster have a primary key" primary key(MON_KEY_NAME)
);

-- il y a des competences de monstre
create table COMPETENCE_MONSTER (
        CFX_ID                  integer NOT NULL,
        CONSTRAINT              "A competence of monster belongs to a competence" foreign key(CFX_ID)
                                REFERENCES COMPETENCE_FX(CFX_ID) DEFERRABLE INITIALLY DEFERRED,
        MON_KEY_NAME           char(10) NOT NULL,
        CONSTRAINT              "A competence of monster belongs to a monster" foreign key(MON_KEY_NAME)
                                REFERENCES MONSTER(MON_KEY_NAME) DEFERRABLE INITIALLY DEFERRED,
        CONSTRAINT              "A competence of MONSTER have a primary key" primary key(CFX_ID, MON_KEY_NAME)
);
create view FX_MONSTER as select * from COMPETENCE_FX natural inner join COMPETENCE_MONSTER;
create function FX_MONSTER_ADD(
                    IN MIN_LEVEL_REQ integer,
                    IN INIT_VALUE integer,
                    IN BONUS_BY_LEVEL float,
                    IN CP_BY_USE integer,
                    IN COMP_KEY_NAME char(5),
                    IN MON_KEY_NAME char(10)
                ) RETURNS void AS $$
DECLARE
    ret RECORD;
    cfx_lastid integer;
BEGIN
    insert into COMPETENCE_FX (CFX_MIN_LEVEL_REQ, CFX_INIT_VALUE, CFX_BONUS_BY_LEVEL, CFX_CP_BY_USE, COMP_KEY_NAME) values
            (MIN_LEVEL_REQ, INIT_VALUE, BONUS_BY_LEVEL, CP_BY_USE, COMP_KEY_NAME) returning CFX_ID into cfx_lastid;
    insert into COMPETENCE_MONSTER values (cfx_lastid, MON_KEY_NAME);
END;
$$ LANGUAGE plpgsql;

create rule FX_MONSTER_INSERT as on INSERT to FX_MONSTER
    do INSTEAD
    (
        select count(FX_MONSTER_ADD(NEW.CFX_MIN_LEVEL_REQ, NEW.CFX_INIT_VALUE, NEW.CFX_BONUS_BY_LEVEL, NEW.CFX_CP_BY_USE, NEW.COMP_KEY_NAME, NEW.MON_KEY_NAME));
    );


-- Mobile Object (un avatar ou un monstre instancié)
--      * position X du Mob
--      * position Y du Mob
--      * valeur d'incrément sur la position X au prochain tour
--      * valeur d'incrément sur la position Y au prochain tour
--      * valeur courante des Hit Points HP
--      * valeur courante des Competences Points CP
create table MOB (
        MOB_ID                      serial NOT NULL,
        MOB_POS_X                   integer NOT NULL,
        MOB_POS_Y                   integer NOT NULL,
        MOB_FUEL                    integer NOT NULL,
        MOB_NEXT_X                  integer NOT NULL,
        MOB_NEXT_Y                  integer NOT NULL,
        MOB_CURRENT_HP              integer NOT NULL,
        MOB_CURRENT_CP              integer NOT NULL,
        CONSTRAINT                  "A mob have a primary key" primary key(MOB_ID),
        MAP_ID                      integer NOT NULL,
        CONSTRAINT                  "A mob belongs to a Map" foreign key(MAP_ID) REFERENCES MAP(MAP_ID)
);

-- tresor présent sur le terrain de jeu ou porter par un monstre
--      * valeur d'éxpérience du trésor
--      * valeur en piéce d'or du trésor
--      * position X du trésor
--      * position Y du trésor
create table TREASURE (
        TRS_ID                      serial NOT NULL,
        TRS_EXP                     integer,
        TRS_PO                      integer,
        TRS_POS_X                   integer,
        TRS_POS_Y                   integer,
        CONSTRAINT                  "A Treasure have a primary key" primary key(TRS_ID),
        MAP_ID                      integer NOT NULL,
        CONSTRAINT                  "A Treasure belongs to a Map" foreign key(MAP_ID) REFERENCES MAP(MAP_ID) DEFERRABLE INITIALLY DEFERRED,
        MOB_ID                      integer DEFAULT NULL,
        CONSTRAINT                  "A Treasure could be wear by a monster" foreign key(MOB_ID) REFERENCES MOB(MOB_ID) DEFERRABLE INITIALLY DEFERRED
);

-- Les joueurs sur la carte
create table PLAYINGAVATAR (
    MOB_ID                      serial NOT NULL,
    CONSTRAINT                  "A PlayingAvatar is a Mob" foreign key(MOB_ID) REFERENCES MOB(MOB_ID),
    AV_ID                       serial NOT NULL,
    CONSTRAINT                  "A PlayingAvatar is also an Avatar" foreign key(AV_ID) REFERENCES AVATAR(AV_ID),
    CONSTRAINT                  "A PlayingAvatar have a primary key" primary key(MOB_ID, AV_ID)
);

-- Les monstres sur la carte
create table PLAYINGMONSTER (
    MOB_ID                      serial NOT NULL,
    CONSTRAINT                  "A PlayingMonster is a Mob" foreign key(MOB_ID) REFERENCES MOB(MOB_ID),
    MON_KEY_NAME                char(10) NOT NULL,
    CONSTRAINT                  "A PlayingMonster is also a Monster" foreign key(MON_KEY_NAME) REFERENCES MONSTER(MON_KEY_NAME),
    CONSTRAINT                  "A PlayingMonster have a primary key" primary key(MOB_ID, MON_KEY_NAME)
);

-- -- Item
-- create table ITEM (
-- );
-- 
-- -- Equipement
-- create table BAG (
-- );
-- 
