----- donnée de test
\echo JEU DE TEST

-- quelques joueurs
insert into ACCOUNT values
    ('theo', 'jarmin', 'theodore', '1ow0jsc8n', 'theo823@gmail.com'),
    ('jiji', 'hijom', 'juliette', '21#!@PL!@', 'jul.hijom@free.fr'),
    ('hckz', 'hacheman', 'joe', '11111', '_1234@free.fr'),
    ('iopi', 'auroux', 'lionel', '213l1i8j12[;', 'lionel.auroux@gmail.com');

-- races de base
insert into RACE values
    ('ELF', 'elf des forets', 1700, 6, 12, 1.8, 2.5),
    ('GNOME', 'gnome des montagnes du nord', 1500, 1, 15, 1.5, 2.5),
    ('HIELF', 'haut elf du moldor', 2100, 6, 16, 1.8, 3.0),
    ('ORC', 'orc', 2000, 15, 5, 3.1, 1.5),
    ('HORC', 'demi-orc', 1500, 10, 6, 2.8, 2.0),
    ('DWARF', 'nain', 1500, 9, 7, 2.6, 2.8),
    ('HUMAN', 'humain de base', 1500, 8, 8, 2.0, 2.0);

-- classes de base
insert into CLASS values
    ('THIEF', 'Voleur', 1500, 8, 8, 1.5, 2.0),
    ('NINJA', 'Ninja', 1500, 8, 8, 1.6, 2.2),
    ('WARIO', 'Guerrier', 1500, 10, 6, 3.0, 2.6),
    ('PALDN', 'Paladin', 1500, 10, 6, 2.8, 2.8),
    ('CLERC', 'Pretre', 1500, 6, 10, 2.2, 3.0),
    ('ELEM', 'Elementaliste', 1500, 6, 10, 2.2, 3.1),
    ('WIZ', 'Magicien', 2100, 3, 16, 1.8, 3.5),
    ('BARBN', 'Barbare', 1900, 15, 6, 3.5, 1.8),
    ('AVEN', 'Aventurier standard', 1500, 8, 8, 2.2, 2.2);

-- quelques restriction
insert into RESTRICTION_CLASS_RACE (CLS_KEY_NAME, RACE_KEY_NAME, RCR_DESC) values
    ('BARBN', 'HIELF', 'trop discipliné'),
    ('BARBN', 'ELF', 'trop discipliné'),
    ('BARBN', 'DWARF', E'ne peut se passer d\'armure'),
    ('PALDN', 'ORC', 'trop chaotique'),
    ('PALDN', 'GNOME', 'trop chaotique'),
    ('NINJA', 'DWARF', 'pas assez souple'),
    ('NINJA', 'HORC', 'pas assez souple'),
    ('NINJA', 'ORC', 'pas assez souple'),
    ('ELEM', 'HORC', 'pas assez intelligent'),
    ('ELEM', 'ORC', 'pas assez intelligent'),
    ('WIZ', 'HORC', 'pas assez intelligent'),
    ('WIZ', 'ORC', 'pas assez intelligent');

-- quelques PJ
insert into AVATAR (AV_NAME, AV_ACTIF, AV_EXP, AV_LEVEL, AV_LAST_HP, AV_LAST_CP,
                    ACC_LOGIN, CLS_KEY_NAME, RACE_KEY_NAME) values
    ('altariel', TRUE, 16400, 4, 19, 30, 'jiji', 'WIZ', 'ELF'),
    ('ziguidi', FALSE, 1000, 1, 4, 20, 'theo', 'NINJA', 'GNOME'),
    ('alfred de blanche-croix', TRUE, 16000, 5, 32, 32, 'theo', 'PALDN', 'HUMAN'),
    ('gros bill', TRUE, 170000, 40, 150, 276, 'hckz', 'WIZ', 'HIELF'),
    ('munchkin', TRUE, 170000, 43, 312, 154, 'hckz', 'BARBN', 'ORC'),
    ('gunther', FALSE, 22100, 7, 48, 45, 'iopi', 'WARIO', 'DWARF'),
    ('grudu', TRUE, 22000, 5, 48, 22, 'iopi', 'BARBN', 'ORC');

-- competences commune
insert into COMPETENCE values
    ('SWORD', 'Epee standard', 'competence sur les epees', 'pour utiliser les items correspondant'),
    ('SABER', 'Sabre standard', 'competence sur les sabres', 'pour utiliser les items correspondant'),
    ('DAG', 'Dague standard', 'competence sur les dagues', 'pour utiliser les items correspondant'),
    ('MASS', 'Masse standard', 'competence sur les casses têtes', 'pour utiliser les items correspondant'),
    ('BATON', 'Gourdin/baton standard', 'competence sur les gourdin/batons', 'pour utiliser les items correspondant'),
    ('AXE', 'Hache standard', 'competence sur les haches', 'pour utiliser les items correspondant'),
    ('CUIR', 'Cuir standard', 'competence sur les armures de cuirs', 'pour utiliser les items correspondant'),
    ('PLATE', 'Plate standard', 'competence sur les armures de plates', 'pour utiliser les items correspondant'),
    ('SHLD', 'Bouclier standard', 'competence sur les boucliers', 'pour utiliser les items correspondant'),
    ('WAND', 'Baguette Magique', 'competence sur les baguettes', 'pour utiliser les items correspondant'),
    --
    ('HTH', 'Hand-to-Hand', 'combat au corps a corps - pour le frapper un enemi proche', 'a utiliser pour les monstres'),
    ('RANGE', 'Ranged Attack', 'combat a distance - pour le frapper un enemi loin', 'a utiliser pour les monstres'),
    --
    --
    ('STR', 'Force', E'Pour savoir combien on peut porter d\'objet', 'utile pour faire plus de dégât'),
    ('MOVE', 'Deplacement', 'Pour les actions necessitant un deplacement', 'quand deplacement sur terrain type terre'),
    ('SWIM', 'Natation', 'Pour les actions necessitant un jet de nage', 'quand deplacement sur terrain type eau')
    ;

-- competence de RACE
insert into FX_RACE (RACE_KEY_NAME, COMP_KEY_NAME, CFX_MIN_LEVEL_REQ, CFX_INIT_VALUE, CFX_BONUS_BY_LEVEL, CFX_CP_BY_USE) values
    -- l'humain est le standard de reference
    ('HUMAN', 'STR', 0, 11, 0.5, 0),
    ('HUMAN', 'MOVE', 0, 12, 0.5, 0),
    ('HUMAN', 'SWIM', 0, 13, 0.5, 0),
    -- un elf c'est pas super costaud
    ('ELF', 'STR', 0, 8, 0.5, 0),
    -- par contre ca galope
    ('ELF', 'MOVE', 0, 12, 0.5, 0),
    -- et ca nage pas trop mal
    ('ELF', 'SWIM', 0, 11, 0.5, 0),
    -- un haut elf, c'est comme un elf
    ('HIELF', 'STR', 0, 8, 0.5, 0),
    ('HIELF', 'MOVE', 0, 12, 0.5, 0),
    ('HIELF', 'SWIM', 0, 11, 0.5, 0),
    -- un gnome c'est tout petit
    ('GNOME', 'STR', 0, 5, 0.5, 0),
    -- mais ca galope tres vite
    ('GNOME', 'MOVE', 0, 12, 0.5, 0),
    -- c plus dur de nager
    ('GNOME', 'SWIM', 0, 9, 0.5, 0),
    -- un orc c'est bourrin
    ('ORC', 'STR', 0, 15, 0.5, 0),
    -- ca cours bien
    ('ORC', 'MOVE', 0, 11, 0.5, 0),
    -- mais ca nage comme un caillou
    ('ORC', 'SWIM', 0, 6, 0.5, 0),
    -- un demi-orc c'est presque un orc
    ('HORC', 'STR', 0, 13, 0.5, 0),
    ('HORC', 'MOVE', 0, 11, 0.5, 0),
    ('HORC', 'SWIM', 0, 8, 0.5, 0),
    -- un nain c'est solide
    ('DWARF', 'STR', 0, 14, 0.5, 0),
    -- c'est pas un bolide
    ('DWARF', 'MOVE', 0, 8, 0.5, 0),
    -- nage comme un caillou avec son pote l'orc...non pas son pote
    ('DWARF', 'SWIM', 0, 6, 0.5, 0)
    ;
---- competences specifiqinsert into FX_RACE (RACE_KEY_NAME, COMP_KEY_NAME, CFX_MIN_LEVEL_REQ, CFX_INIT_VALUE, CFX_BONUS_BY_LEVEL, CFX_CP_BY_USE) values ('HUMAN', 'STR', 0, 10, 1, 0);ues
insert into FX_CLS (COMP_KEY_NAME, CFX_MIN_LEVEL_REQ, CFX_INIT_VALUE, CFX_BONUS_BY_LEVEL, CFX_CP_BY_USE, CLS_KEY_NAME) values
    -- epee
    ('SWORD', 1, 10, 1.5, 1,  'WARIO'),
    ('SWORD', 1, 8, 0.8, 1, 'PALDN'),
    ('SWORD', 1, 6, 1.3, 1, 'BARBN'),
    ('SWORD', 1, 5, 0.5, 2, 'AVEN'),
    ('SWORD', 2, 8, 1.2, 2, 'NINJA'),
    ('SWORD', 2, 8, 0.6, 2, 'THIEF'),
    ('SWORD', 2, 5, 0.5, 3, 'ELEM'),
    ('SWORD', 3, 4, 0.4, 3, 'CLERC'),
    -- sabre
    ('SABER', 1, 12, 1.6, 2, 'NINJA'),
    ('SABER', 1, 10, 1.5, 2, 'WARIO'),
    ('SABER', 6, 10, 0.5, 2, 'AVEN'),
    -- dague
    ('DAG', 1, 10, 1.5, 1, 'THIEF'),
    ('DAG', 1, 10, 1.6, 1, 'NINJA'),
    ('DAG', 1, 8, 0.6, 2, 'WIZ'),
    ('DAG', 1, 8, 0.5, 2, 'ELEM'),
    ('DAG', 1, 8, 0.9, 2, 'BARBN'),
    ('DAG', 1, 8, 1.1, 2, 'WARIO'),
    ('DAG', 2, 8, 0.6, 2, 'AVEN'),
    -- masse
    ('MASS', 1, 10, 1.3, 1,  'CLERC'),
    ('MASS', 1, 8, 1.2, 1, 'PALDN'),
    ('MASS', 1, 8, 1.1, 2, 'WARIO'),
    ('MASS', 3, 4, 0.8, 2, 'AVEN'),
    -- gourdin/baton
    ('BATON', 1, 10, 1.5, 1, 'THIEF'),
    ('BATON', 1, 10, 1.6, 1, 'NINJA'),
    ('BATON', 1, 9, 0.5, 1, 'AVEN'),
    ('BATON', 1, 8, 0.4, 1, 'WIZ'),
    ('BATON', 1, 8, 0.5, 1, 'ELEM'),
    -- hache
    ('AXE', 1, 12, 1.8, 1, 'BARBN'),
    ('AXE', 1, 12, 1.3, 1, 'WARIO'),
    ('AXE', 4, 10, 0.8, 2, 'AVEN'),
    -- armure de cuir
    ('CUIR', 1, 10, 0.4, 1, 'BARBN'),
    ('CUIR', 1, 10, 1.5, 1, 'THIEF'),
    ('CUIR', 2, 8, 0.2, 2, 'AVEN'),
    -- armure de plate
    ('PLATE', 1, 12, 1.6, 1, 'WARIO'),
    ('PLATE', 1, 10, 1.4, 2, 'PALDN'),
    ('PLATE', 1, 10, 1.5, 2, 'CLERC'),
    ('PLATE', 6, 10, 1.4, 2, 'AVEN'),
    -- bouclier
    ('SHLD', 1, 12, 1.8, 1, 'WARIO'),
    ('SHLD', 1, 10, 1.4, 2, 'PALDN'),
    ('SHLD', 5, 10, 1.4, 3, 'AVEN'),
    -- baguette magique
    ('WAND', 2, 10, 1.2, 2, 'ELEM'),
    ('WAND', 1, 12, 1.4, 1, 'WIZ'),
    ('WAND', 5, 6, 0.1, 4, 'AVEN')
    ;

insert into MAP values
    (DEFAULT, 100, 100, 'zone de debut'),
    (DEFAULT, 1000, 1500, 'monde 1'),
    (DEFAULT, 1200, 1600, 'monde 2')
    ;

insert into MONSTER values
    ('SKELETON', 100, 10, 15, 0, 0),
    ('BAT', 50, 5, 8, 0, 0),
    ('UNDEAD', 500, 50, 80, 0, 0)
    ;

insert into FX_MONSTER values
    (DEFAULT, 0, 10, 0, 0, 'MOVE', 'SKELETON')
    ;
